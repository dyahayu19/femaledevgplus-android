/**
 * 
 */
package com.femaledev.gplus;

import com.google.android.gms.plus.PlusShare;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

/**
 * @author oonarfiandwi
 *
 */
public class ShareTextActivity extends Activity {
	
	private EditText mSharedText;
	private EditText mContentUrl;
	private static final int REQUEST_CODE_SHARETEXT = 0; // XXX what happen if we change to 3?
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.share_text_activity);
		
		mSharedText = (EditText) findViewById(R.id.shared_text);
		mContentUrl = (EditText) findViewById(R.id.shared_url);
		Button shareButton = (Button) findViewById(R.id.share_button);
		shareButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivityForResult(getShareTextIntent(
						mSharedText.getText().toString(), mContentUrl.getText().toString()),
						REQUEST_CODE_SHARETEXT);
			}
		});
	}
	
	
	protected Intent getShareTextIntent(String text, String contentUrl) {
		PlusShare.Builder builder = new PlusShare.Builder(this);
		builder.setType("text/plain");
		builder.setText(text);
		if(!contentUrl.equals("")) {
			builder.setContentUrl(Uri.parse(contentUrl));
		}
		return builder.getIntent();
	}
}
