/**
 * 
 */
package com.femaledev.gplus;

import com.google.android.gms.plus.PlusShare;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

/**
 * @author oonarfiandwi
 *
 */
public class ShareMediaActivity extends Activity {
	
	private static final int REQ_SELECT_PHOTO = 1;
	private static final int REQ_START_SHARE = 2;
	
	private EditText mSharedText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.share_media_activity);
		
		
		mSharedText = (EditText) findViewById(R.id.shared_text);
		
		Button chooseMediaButton = (Button) findViewById(R.id.choose_media);
		chooseMediaButton.setOnClickListener(new android.view.View.OnClickListener() {
		      public void onClick(View v) {
		        Intent photoPicker = new Intent(Intent.ACTION_PICK);
		        photoPicker.setType("video/*, image/*");
		        startActivityForResult(photoPicker, REQ_SELECT_PHOTO);
		      }
		    });
		
	}
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
	  super.onActivityResult(requestCode, resultCode, intent);

	  if(requestCode == REQ_SELECT_PHOTO) {
	    if(resultCode == RESULT_OK) {
	      Uri selectedImage = intent.getData();
	      ContentResolver cr = this.getContentResolver();
	      String mime = cr.getType(selectedImage);

	      PlusShare.Builder share = new PlusShare.Builder(this);
	      share.setText(mSharedText.getText().toString());
	      share.addStream(selectedImage);
	      share.setType(mime);
	      startActivityForResult(share.getIntent(), REQ_START_SHARE);
	    }
	  }
	}
}
