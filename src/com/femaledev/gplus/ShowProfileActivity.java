/**
 * 
 */
package com.femaledev.gplus;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import com.femaledev.gplus.util.DownloadImageTask;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.Person.Image;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author oonarfiandwi
 *
 */
public class ShowProfileActivity extends Activity 
implements PlusClient.ConnectionCallbacks,
PlusClient.OnConnectionFailedListener,
PlusClient.OnAccessRevokedListener {

	private static int MY_ACTIVITYS_AUTH_REQUEST_CODE = 1;

	private PlusClient mPlusClient;
	private TextView mSignInStatus;
	private ImageView mPersonImage;
	private TextView mPageProfileInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_profile_activity);

		mPlusClient = new PlusClient.Builder(this, this, this)
		.setActions(MomentUtil.ACTIONS)
		.build();

		mSignInStatus = (TextView) findViewById(R.id.sign_in_status);
		mPersonImage = (ImageView) findViewById(R.id.person_image);
		mPageProfileInfo = (TextView) findViewById(R.id.page_profile_info);
	}

	protected void loadCurrentProfile() {
		Person currentPerson = mPlusClient.getCurrentPerson();
		mSignInStatus.setText(currentPerson.getDisplayName() +
				" " + mPlusClient.getAccountName() + 
				" Page O:" + currentPerson.getCircledByCount() +
				" Page +1:" +
				currentPerson.getPlusOneCount());
		mPersonImage.setVisibility(View.VISIBLE);
		Image personImage = currentPerson.getImage();
		String personImageUrl = personImage.getUrl();
		if(personImageUrl.indexOf("?sz=")>-1) {
			personImageUrl = personImageUrl.substring(0, personImageUrl.indexOf("?sz=")) + "?sz=256";
		}
		new DownloadImageTask(mPersonImage).execute(personImageUrl);
	}

	//	protected void loadPageProfile(final String id) {
	//		final String accountName = mPlusClient.getAccountName();
	//		new AsyncTask<Void, Void, String>() {
	//			@Override
	//			protected String doInBackground(Void... params) {
	//				HttpURLConnection urlConnection = null;
	//
	//				try {
	//					URL url = new URL("https://www.googleapis.com/plus/v1/people/"+id);
	//					String sAccessToken = GoogleAuthUtil.getToken(ShowProfileActivity.this, accountName,
	//							"oauth2:" + Scopes.PLUS_LOGIN +" " + Scopes.PROFILE );
	//					//		    	              + " https://www.googleapis.com/auth/plus.profile.emails.read");
	//					urlConnection = (HttpURLConnection) url.openConnection();
	//					urlConnection.setRequestProperty("Authorization", "Bearer " + sAccessToken);
	//					BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
	//					StringBuffer sb = new StringBuffer();
	//					String st;
	//					while((st=br.readLine()) != null) {
	//						sb.append(st);
	//					}
	//
	//					if(sb.length()>1) {
	//						///Log.w("XXX", sb.toString());
	//						return sb.toString();
	//					}
	//
	//					// String content = CharStreams.toString(new InputStreamReader(urlConnection.getInputStream(), Charsets.UTF_8));
	//				} catch (MalformedURLException e) {
	//					Log.e("PageProfile", "MalformedURLException: "+e.getMessage());
	//				} catch (UserRecoverableAuthException e) {
	//					Log.e("PageProfile", "UserRecoverableAuthException: "+e.getMessage());
	//					// Start the user recoverable action using the intent returned by
	//					// getIntent()
	//					ShowProfileActivity.this.startActivityForResult(
	//							e.getIntent(), MY_ACTIVITYS_AUTH_REQUEST_CODE);
	//					return null;
	//				} catch (GoogleAuthException e) {
	//					Log.e("PageProfile", "GoogleAuthException: "+e.getMessage());
	//				} catch (IOException e) {
	//					Log.e("PageProfile", "IOException: "+e.getMessage());
	//				} finally {
	//					if(urlConnection != null) {
	//						urlConnection.disconnect();
	//					}
	//				}
	//
	//				return null;
	//			}
	//
	//			@Override
	//			protected void onPostExecute(String info) {
	//				if(info.length()>1) {
	//					try {
	//						JSONObject jObj = new JSONObject(info);
	//						mPageProfileInfo.setText(jObj.getString("displayName") +
	//								" Page O:" + jObj.getString("circledByCount") +
	//								" Page +1:" + jObj.getString("plusOneCount"));
	//
	//
	//					} catch (JSONException e) {
	//						Log.e("PageProfile", "JSONException: "+e.getMessage());
	//					}
	//				}
	//			}
	//		}.execute();
	//	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == MY_ACTIVITYS_AUTH_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				/// XXX TODO ???!!! 
				///            	loadPageProfile("111198267224714750335");
			}
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		Toast.makeText(this, "start", Toast.LENGTH_LONG).show();
		mPlusClient.connect();
	}

	@Override
	public void onStop() {
		Toast.makeText(this, "stop", Toast.LENGTH_LONG).show();
		mPlusClient.disconnect();
		super.onStop();
	}

	@Override
	public void onAccessRevoked(ConnectionResult status) {
		Toast.makeText(this, "access revoked", Toast.LENGTH_LONG).show();
		if (status.isSuccess()) {
			mSignInStatus.setText(R.string.revoke_access_status);
		} else {
			mSignInStatus.setText(R.string.revoke_access_error_status);
			mPlusClient.disconnect();
		}
		mPlusClient.connect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		///mConnectionResult = result;
		Toast.makeText(this, "connection failed", Toast.LENGTH_LONG).show();
		mSignInStatus.setText(R.string.not_sign_in);
		mPersonImage.setVisibility(View.INVISIBLE);
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		Toast.makeText(this, "connected", Toast.LENGTH_LONG).show();
		if(mPlusClient.getCurrentPerson() != null) {
			loadCurrentProfile();
			//			loadPageProfile("111198267224714750335");
		}
		else {
			mSignInStatus.setText(getString(R.string.unknown_person));
		}
	}

	@Override
	public void onDisconnected() {
		Toast.makeText(this, "disconnected", Toast.LENGTH_LONG).show();
		mSignInStatus.setText(R.string.loading_status);
		mPersonImage.setVisibility(View.INVISIBLE);
		mPlusClient.connect();
	}



}
