package com.femaledev.gplus;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        
        Button btn_goto_login = (Button) findViewById(R.id.btn_goto_login);
        btn_goto_login.setOnClickListener(this);
        
        Button btn_goto_profile = (Button) findViewById(R.id.btn_goto_profile);
        btn_goto_profile.setOnClickListener(this);
        
        Button btn_goto_sharetext = (Button) findViewById(R.id.btn_goto_sharetext);
        btn_goto_sharetext.setOnClickListener(this);
        
        Button btn_goto_sharemedia = (Button) findViewById(R.id.btn_goto_sharemedia);
        btn_goto_sharemedia.setOnClickListener(this);
        
        Button btn_goto_interactivepost = (Button) findViewById(R.id.btn_goto_interactivepost);
        btn_goto_interactivepost.setOnClickListener(this);
    }

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.btn_goto_login:
			Intent intent_login = new Intent(this, SignInActivity.class);
			startActivity(intent_login);
			break;
		case R.id.btn_goto_profile:
			Intent intent_profile = new Intent(this, ShowProfileActivity.class);
			startActivity(intent_profile);
			break;
		case R.id.btn_goto_sharetext:
			Intent intent_sharetext = new Intent(this, ShareTextActivity.class);
			startActivity(intent_sharetext);
			break;
		case R.id.btn_goto_sharemedia:
			Intent intent_sharemedia = new Intent(this, ShareMediaActivity.class);
			startActivity(intent_sharemedia);
			break;
		case R.id.btn_goto_interactivepost:
			Intent intent_interactivepost = new Intent(this, InteractivePostActivity.class);
			startActivity(intent_interactivepost);
			break;
		}
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


}
