/**
 * 
 */
package com.femaledev.gplus;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.PlusShare;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

/**
 * @author oonarfiandwi
 *
 */
public class InteractivePostActivity extends Activity implements OnClickListener,
		PlusClient.ConnectionCallbacks, PlusClient.OnConnectionFailedListener,
		PlusClient.OnAccessRevokedListener {
	
    private static final int REQUEST_CODE_INTERACTIVE_POST = 1;
    
    private PlusClient mPlusClient;
    private ImageButton mShareButton;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interactive_post);
		
        mPlusClient = new PlusClient.Builder(this, this, this)
        .build();

        mShareButton = (ImageButton) findViewById(R.id.interactive_button);
        mShareButton.setOnClickListener(this);
		
	}
    
    @Override
    protected void onStart() {
    	super.onStart();
    	mPlusClient.connect();
    }
    
    @Override
    protected void onStop() {
    	super.onStop();
    	mPlusClient.disconnect();
    	mShareButton.setEnabled(false);
    }

	@Override
	public void onAccessRevoked(ConnectionResult status) {
//		Toast.makeText(this, "access revoked", Toast.LENGTH_LONG).show();
//        if (!status.isSuccess()) {
//            mPlusClient.disconnect();
//        }
//        mPlusClient.connect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		mShareButton.setEnabled(false);
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		mShareButton.setEnabled(true);
	}

	@Override
	public void onDisconnected() {
		mShareButton.setEnabled(false);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.interactive_button:
			PlusShare.Builder builder = new PlusShare.Builder(this);
			
			// Set call-to-action metadata.
		    builder.addCallToAction(
		          "VIEW_PROFILE", /** call-to-action button label */
		          Uri.parse("https://plus.google.com/116226722391771006781"), /** call-to-action url (for desktop use) */
		          "/116226722391771006781" /** call to action deep-link ID (for mobile use), 512 characters or fewer */);

		    // Set the content url (for desktop use).
		    builder.setContentUrl(Uri.parse("http://youtube.com/watch?v=9ZjbqUWag7s"));

		    // Set the target deep-link ID (for mobile use).
		    builder.setContentDeepLinkId("/watch?v=",
		              null, null, null);

		    // Set the share text.
		    builder.setText("The artist of Action Guru - The Duelist, +116226722391771006781 ");

		    startActivityForResult(builder.getIntent(), REQUEST_CODE_INTERACTIVE_POST);
			break;
		}
	}
	
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode) {
            case REQUEST_CODE_INTERACTIVE_POST:
                if (resultCode != RESULT_OK) {
                    Log.e("InteractiveP", "Failed to create interactive post");
                }
                break;
        }
    }

	
//    private Intent getInteractivePostIntent() {
//        // Create an interactive post with the "VIEW_ITEM" label. This will
//        // create an enhanced share dialog when the post is shared on Google+.
//        // When the user clicks on the deep link, ParseDeepLinkActivity will
//        // immediately parse the deep link, and route to the appropriate resource.
//        String action = "/?view=true";
//        Uri callToActionUrl = Uri.parse(getString(R.string.plus_example_deep_link_url) + action);
//        String callToActionDeepLinkId = getString(R.string.plus_example_deep_link_id) + action;
//
//        // Create an interactive post builder.
//        PlusShare.Builder builder = new PlusShare.Builder(this);
//
//        // Set call-to-action metadata.
//        builder.addCallToAction(LABEL_VIEW_ITEM, callToActionUrl, callToActionDeepLinkId);
//
//        // Set the target url (for desktop use).
//        builder.setContentUrl(Uri.parse(getString(R.string.plus_example_deep_link_url)));
//
//        // Set the target deep-link ID (for mobile use).
//        builder.setContentDeepLinkId(getString(R.string.plus_example_deep_link_id),
//                null, null, null);
//
//        // Set the pre-filled message.
//        builder.setText(mEditSendText.getText().toString());
//
//        return builder.getIntent();
//    }

}
