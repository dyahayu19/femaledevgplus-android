/**
 * 
 */
package com.femaledev.gplus;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;


/**
 * @author dyahayususilowati
 *
 */
public class SplashActivity extends Activity {

	int splashTime = 4000;
	boolean isShowing = true;
	Thread splashThread;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		ImageView imageSplash = (ImageView) findViewById(R.id.image_logo);

		Animation fading = AnimationUtils.loadAnimation(this, R.anim.fade);
		imageSplash.startAnimation(fading);
		DisplayMetrics displaymetrics = new DisplayMetrics();	
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);	

		startSplash();
	}

	@Override
	public void onStop() {
		super.onStop();
	}


	private synchronized void startSplash(){
		if(splashThread==null){
			splashThread = new Thread() {
				@Override
				public void run() {

					try {
						int waited = 0;
						while(isShowing && waited < splashTime) {
							sleep(1000);
							waited += 1000;
						}
					} 
					catch(InterruptedException e) {
						e.printStackTrace();
						System.out.println("what happen here???");
					} 
					finally {
						SplashActivity.this.finish();
						Intent i = new Intent(SplashActivity.this, MainActivity.class);
						startActivity(i);
						stopSplash();
					}
				}
			};
			splashThread.start();
		}
	}

	public synchronized void stopSplash(){
		if(splashThread != null){
			Thread thread = splashThread;
			splashThread = null;
			thread.interrupt();
		}
	}       

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(event.getAction() == MotionEvent.ACTION_DOWN){
			isShowing = false;
		}
		return super.onTouchEvent(event);
	}
}